package com.exadiawinds.exadiabank;

import java.text.DecimalFormat;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

public class GroupCommandExecutor implements CommandExecutor {
	
	private Plugin eb = exadiabank.getInstance();
		
	// onCommand is going to dispatch right to DataAccess, which will take over and handle all the logic and success/error messages
	// The biggest reason why I'm using a dispatcher is so methods can easily be worked on and updated in the future (read: When UUID comes out)
	//
	// NOTE: I have a funny feeling that the target player isn't being set right here...
	//       Might need to review how I'm using 'sender' vs 'player'...
	//
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage("This command can only be ran by a player!");
			return false;
		} else {
			if (args.length < 1) {
				return false;
			}
			String subcmd = args[0].toLowerCase();
			String player = sender.getName();
			String uuid = _getPlayerUUID(player);
			
			switch (subcmd) {
			case "new":
				if (args.length != 2 || !(args[1].matches("[a-z0-9]+")) || args[1].length() > 16) {
					sender.sendMessage("Usage: /ebg new <group_bank_name>");
					sender.sendMessage("Example: /ebg new mygroup");
					sender.sendMessage("Note: <group_bank_name> must be lower case, no spaces or symbols");
				} else {
					DataAccess.AddNewGroup(args[1], player, sender);
					DataAccess.AddPlayerToGroup(args[1], player, sender);
					DataAccess.ChangeGroupOwnership(args[1], player, sender, true); // the 4th param bypasses owner check
				}
				return true;
			case "delete":
				if (args.length != 2 || !(args[1].matches("[a-z0-9]+")) || args[1].length() > 16) {
					sender.sendMessage("Usage: /ebg delete <group_bank_name>");
					sender.sendMessage("Example: /ebg delete mygroup");
				} else {
					// DataAccess call to remove a group
				}
				return true;
			case "makeowner":
				// args: 0=subcmd, 1=group_name, 2=member_name
				if (args.length != 3 || !(args[1].matches("[a-z0-9]+")) || args[1].length() > 16 || !(args[2].matches("[a-z0-9_]+")) || args[2].length() > 16) {
					sender.sendMessage("Usage: /ebg makeowner <group_bank_name> <member_name>");
					sender.sendMessage("Example: /ebg makeowner mygroup CoolPlayer");
				} else {
					DataAccess.ChangeGroupOwnership(args[1], args[2], sender);
				}
				return true;
			case "removemember":
				// args: 0=subcmd, 1=group_name, 2=member_name
				if (args.length != 3 || !(args[1].matches("[a-z0-9]+")) || args[1].length() > 16 || !(args[2].matches("[a-z0-9_]+")) || args[2].length() > 16) {
					sender.sendMessage("Usage: /ebg removemember <group_bank_name> <member_name>");
					sender.sendMessage("Example: /ebg removemember mygroup CoolPlayer");
				} else {
					DataAccess.RemoveMemberFromGroup(args[1], args[2], sender);
				}
				return true;
			case "leavegroup":
				// args: 0=subcmd, 1=group_name
				if (args.length != 2 || !(args[1].matches("[a-z0-9]+")) || args[1].length() > 16) {
					sender.sendMessage("Usage: /ebg leavegroup <group_bank_name>");
					sender.sendMessage("Example: /ebg leavegroup mygroup");
				} else {
					DataAccess.RemoveSelfFromGroup(args[1], sender.getName(), sender);
				}
				return true;
			}
			return true;
		}
	}
	private static String _getPlayerUUID(String player) {
		// This private method will be used to get the player's Mojang Universally Unique ID
		// Mojang is going to require this in the future. I'm coding in anticipation for it (I hope...)
		return "";
	}
}
