package com.exadiawinds.exadiabank;

import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

public final class exadiabank extends JavaPlugin {

	private static Plugin instance;
	
	public void onEnable() {
		instance = this;
		
		// Inject our commands into Bukkit
		getCommand("eb").setExecutor(new SingleCommandExecutor());
        getCommand("ebg").setExecutor(new GroupCommandExecutor());
    
        // Initialize the database
        boolean dbSchemaExist = DataAccess.InitDatabase();
        if (dbSchemaExist == true) {
        	getLogger().info("Database schema exists or was created successfully.");
        } else {
        	getLogger().warning("Unable to create database schema!");
        }
        
        // Initialize the tables
        boolean dbTablesExist = DataAccess.InitTables();
        if (dbTablesExist == true) {
        	getLogger().info("Tables exist or were created successfully.");
        } else {
        	getLogger().warning("Unable to create database tables!");;
        }
    
	}
	
	public void onDisable() {
	}

	public static Plugin getInstance() {
		return instance;
	}
}
