package com.exadiawinds.exadiabank;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.sql.*;
import java.text.DecimalFormat;

public class DataAccess {
	// Eventually these can be read from a config.yml file!
	private static String dbHostname = "localhost";
	private static Integer dbPortNum = 0;
	private static String dbDatabase = "exadiabank";
	private static String dbUsername = "root";
	private static String dbPassword = "";
	// Need to research what it would take to also give the user the option of using an SQLite database
	private static String dbDriver = "com.mysql.jdbc.Driver";
	
	private static Class<DataAccess> instance;
	
	private static Plugin eb = exadiabank.getInstance();
		
	public static Class<DataAccess> getInstance() {
		return instance;
	}
	
	public static boolean InitDatabase() {
		// Create the database if it doesn't already exist
		// The connection details are not abstracted out because we're connecting without a database, whereas the abstracted
		// _getConnection() requires a database schema in its paramater set.
		String create_db_statement = "CREATE DATABASE IF NOT EXISTS `exadiabank`";
		try {
			Class.forName(dbDriver);
			Connection conn = DriverManager.getConnection(String.format("jdbc:mysql://%s", dbHostname), dbUsername, dbPassword);
            conn.createStatement().execute(create_db_statement);
            return true;
		}
		catch (Exception err) {
			eb.getLogger().warning(err.toString());
			return false;
		}
	}
	
	public static boolean InitTables() {
		// Create the tables if they don't already exist
		String create_table_groups = "CREATE TABLE IF NOT EXISTS `groups` ("
			+ "`id` INT(16) NOT NULL PRIMARY KEY AUTO_INCREMENT, "
			+ "`name` VARCHAR(16) NOT NULL, "
			+ "`money` DECIMAL(10,2) NOT NULL DEFAULT 0.00, "
			+ "`xp` INT(16) NOT NULL DEFAULT 0, "
			+ "`is_active` BOOLEAN NOT NULL DEFAULT 1, "
			+ "`is_banker_required` BOOLEAN NOT NULL DEFAULT 0, "
			+ "`created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP, "
			+ "`updated_at` TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, "
			+ "UNIQUE (`name`))";
		String create_table_group_users = "CREATE TABLE IF NOT EXISTS `group_users` ("
			+ "`id` INT(16) NOT NULL PRIMARY KEY AUTO_INCREMENT, "
			+ "`name` VARCHAR(16) NOT NULL, "
			+ "`uuid` CHAR(36) NULL, "
			+ "`group_id` INT(16) NOT NULL, "
			+ "`is_owner` BOOLEAN NOT NULL DEFAULT 0, "
			+ "`is_banker` BOOLEAN NOT NULL DEFAULT 0, "
			+ "`created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP, "
			+ "`updated_at` TIMESTAMP ON UPDATE CURRENT_TIMESTAMP)";
		String create_table_transactions = "CREATE TABLE IF NOT EXISTS `transactions` ("
			+ "`id` INT(32) NOT NULL PRIMARY KEY AUTO_INCREMENT, "
			+ "`user_id` INT(16) NOT NULL, "
			+ "`group_id` INT(16) NOT NULL, "
			+ "`type` ENUM('xp','money') NOT NULL, "
			+ "`action` ENUM('deposit', 'withdrawl') NOT NULL, "
			+ "`amount` DECIMAL(10,2) NOT NULL, "
			+ "`date` TIMESTAMP DEFAULT CURRENT_TIMESTAMP)";
		// NOTE: `history` table is for internal use only for admins to look up events (not a player-facing data store)
		String create_table_history = "CREATE TABLE IF NOT EXISTS `history` ("
			+ "`id` INT(32) NOT NULL PRIMARY KEY AUTO_INCREMENT, "
			+ "`user_id` INT(16) NOT NULL, "
			+ "`group_id` INT(16) NOT NULL, "
			+ "`action` ENUM('new', 'delete', 'makeowner', 'clearowners', 'addmember', 'removemember', 'addbanker', 'removebanker', 'setflag') NOT NULL, "
			+ "`details` VARCHAR(128) NULL, "
			+ "`date` TIMESTAMP DEFAULT CURRENT_TIMESTAMP)";		
		try {
			Connection conn = _getConnection();
			conn.createStatement().execute(create_table_groups);
            conn.createStatement().execute(create_table_group_users);
            conn.createStatement().execute(create_table_transactions);
            conn.createStatement().execute(create_table_history);
            conn.close();
            return true;
		}
		catch (Exception err) {
			eb.getLogger().warning(err.toString());
			return false;
		}
	}
	
	public static void AddNewGroup(String group, String player, CommandSender sender) {
		String create_new_group = "INSERT INTO groups (name, money, xp, is_banker_required) values (?, ?, ?, ?)";
		Connection conn = _getConnection();
		try {
			final PreparedStatement ps = conn.prepareStatement(create_new_group, Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, group);
			ps.setInt(2, 0);
			ps.setInt(3, 0);
			ps.setInt(4, 0);
			
			final int insertStatus = ps.executeUpdate();
			int newKey = -1;
			if(insertStatus == 1) {
				final ResultSet rs = (ResultSet) ps.getGeneratedKeys();
				if(rs.next()) {
					newKey = rs.getInt(1);
				}
			}
			conn.close();
			eb.getLogger().info(String.format("%s added a new group %s with ID %s", player, group, newKey));
			sender.sendMessage("You have created a new group bank: " + group + "!");
			_addHistory(sender.getName(), group, "new", "");
		}
		catch (Exception err) {
			eb.getLogger().warning(String.format("%s tried to add a new group %s, but it failed:", sender.getName(), group));
			eb.getLogger().warning(err.getMessage());
			sender.sendMessage("Unable to add group bank: " + err.getMessage());
		}
	}
	
	public static void AddPlayerToGroup(String group, String player, CommandSender sender) {
		String add_player_to_group = "INSERT INTO group_users (name, uuid, group_id, is_owner, is_banker) "
			+ "values (?, '', (SELECT id from groups WHERE name = ?), 0, 0)";
		if(_isGroup(group) == false) {
			eb.getLogger().warning(String.format("%s trying to add %s to non-existant group %s", sender.getName(), player, group));
			sender.sendMessage("That group doesn't exist!");
			return;
		}
		Connection conn = _getConnection();
		try {
			final PreparedStatement ps = conn.prepareStatement(add_player_to_group);
			ps.setString(1, player);
			ps.setString(2, group);
			ps.executeUpdate();
			conn.close();
			eb.getLogger().info(String.format("Added player %s to group %s", player, group));
			sender.sendMessage(String.format("Player %s has been added to group %s", player, group));
			_addHistory(sender.getName(), group, "addmember", player);
			Player target = (Bukkit.getServer().getPlayer(player));
			if (target != null) {
				target.sendMessage(String.format("%s has added you to group bank %s",sender.getName(), group));
			}
		}
		catch (Exception err) {
			eb.getLogger().warning(String.format("%s tried to add %s to group %s, but it failed:", sender.getName(), player, group));
			eb.getLogger().warning(err.getMessage());
			sender.sendMessage("Unable to add bank: " + err.getMessage());
		}
	}
	
	public static void RemoveSelfFromGroup(String group, String player, CommandSender sender) {
		RemoveMemberFromGroup(group, sender.getName(), sender);
	}
	
	public static void RemoveMemberFromGroup(String group, String player, CommandSender sender) {
		// Is the caller an Owner? - If so, let them continue
		// Is the caller trying to remove self? - Tell them so (player will equal sender.getName() and will come from LeaveGroup())
		String remove_player_from_group = "DELETE FROM group_users WHERE name = ? and group_id = (SELECT id FROM groups WHERE name = ?)";
		
		if(_isGroup(group) == false) {
			eb.getLogger().warning(String.format("%s trying to remove %s from a non-existant group %s", sender.getName(), player, group));
			sender.sendMessage("That group doesn't exist!");
			return;
		}
		
		PreparedStatement ps;
		if (player == sender.getName()) {
			// Player is removing self
			Connection conn = _getConnection();
			try {
				ps = conn.prepareStatement(remove_player_from_group);
				ps.setString(1, sender.getName());
				ps.setString(2, group);
				ps.execute(remove_player_from_group);
				conn.close();
				eb.getLogger().info(String.format("%s has removed self from %s", sender.getName(), group));
				sender.sendMessage("You have successfully removed yourself from group bank " + group);
				_addHistory(sender.getName(), group, "removemember", "self");
			}
			catch (Exception err) {
				eb.getLogger().warning(String.format("%s tried to remove themselves from group %s, but it failed:", sender.getName(), group));
				eb.getLogger().warning(err.getMessage());
				sender.sendMessage("Unable to remove you from the group: " + err.getMessage());
			}
		} else {
			// Someone is calling to remove a player from a group. Are they an owner?
			if (_isPlayerAOwner(group, player) == true) {
				Connection conn = _getConnection();
				try {
					ps = conn.prepareStatement(remove_player_from_group);
					ps.setString(1, player);
					ps.setString(2, group);
					ps.execute(remove_player_from_group);
					conn.close();
					eb.getLogger().info(String.format("%s has removed %s from %s", sender.getName(), player, group));
					sender.sendMessage(String.format("You have successfully removed %s from group bank %s!", player, group));
					_addHistory(sender.getName(), group, "removemember", player);
				}
				catch (Exception err) {
					eb.getLogger().warning(String.format("%s tried to remove %s from group %s, but it failed:", sender.getName(), player, group));
					eb.getLogger().warning(err.getMessage());
					sender.sendMessage("Unable to remove you from the group: " + err.getMessage());
				}
			} else {
				eb.getLogger().warning(String.format("%s tried to remove %s from %s but is not an owner!", sender.getName(), player, group));
				sender.sendMessage("Permission denied - you are not the owner of " + group);
			}
		}
	}
	
	public static void ChangeGroupOwnership(String group, String player, CommandSender sender) {
		// *** Use this method (3 param) instead of the 4 param main method - safer!
		ChangeGroupOwnership(group, player, sender, false);
	}

	public static void ChangeGroupOwnership(String group, String player, CommandSender sender, boolean bypass_owner_check) {
		// 4th parameter exists so a brand new group bank can be assigned a owner without checking if an owner already exists
		// It would be silly to have 2 sets of code for those 2 possible conditions (new banks and existing banks changing hands)
		// Are you allowed to be doing this...?
		if (bypass_owner_check == false) {
			if (_isPlayerAOwner(group, player) == false) {
				eb.getLogger().info(String.format("%s tried to change ownership of group %s but is not an owner!", sender.getName(), group));
				sender.sendMessage("Permission denied - you are not the owner of " + group);
				return;
			}
		}
		
		// I guess they're allowed to make the change, so let's get busy!
		String demote_owners = "UPDATE group_users SET is_owner = false WHERE group_id = (SELECT id FROM groups WHERE name = ?)";
		String promote_member = "UPDATE group_users SET is_owner = true WHERE name = ? AND group_id = (SELECT id FROM groups WHERE name = ?)";

		if(_isGroup(group) == false) {
			eb.getLogger().warning(String.format("%s trying to change ownership of non-existant group %s", sender.getName(), group));
			sender.sendMessage("That group doesn't exist!");
			return;
		}
		
		// First, make sure that the player we're trying to change ownership to actually exists in the group!
		if (_isPlayerAMember(group, player) == false) {
			eb.getLogger().info(String.format("%s is trying to make %s owner of %s, but player is not a member...", sender.getName(), player, group));
			sender.sendMessage(String.format("%s is not a member of %s!", player, group));
			return;
		}
		
		// Demote everyone from the group!
		try {
			Connection conn = _getConnection();
			final PreparedStatement ps_demote = conn.prepareStatement(demote_owners);
			ps_demote.setString(1, group);
			ps_demote.executeUpdate();
			conn.close();
			eb.getLogger().info(String.format("%s demoted users from %s", sender.getName(), group));
			sender.sendMessage("Owners have been cleared from group " + group);
			_addHistory(sender.getName(), group, "clearowners", "");
		}
		catch (Exception err) {
			eb.getLogger().warning("Unable to demote users for group " + group);
			eb.getLogger().warning(err.getMessage());
			sender.sendMessage("Can't demote the owner: " + err.getMessage());
			return; // If we failed here, then we don't want to continue...
		}
		// Go ahead and promote the player requested
		try {
			Connection conn = _getConnection();
			final PreparedStatement ps_promote = conn.prepareStatement(promote_member);
			ps_promote.setString(1, player);
			ps_promote.setString(2, group);
			ps_promote.executeUpdate();
			conn.close();
			eb.getLogger().info(String.format("%s promoted %s to owner for group %s", sender.getName(), player, group));
			sender.sendMessage(String.format("%s is now the owner of %s", player, group));
			_addHistory(sender.getName(), group, "makeowner", player);
		}
		catch (Exception err) {
			eb.getLogger().warning(String.format("%s can't promote player %s for group %s:", sender.getName(), player, group));
			eb.getLogger().warning(err.getMessage());
			sender.sendMessage("Can't promote member: " + err.getMessage());
		}		
	}
		
	public static boolean PayXPToMember(String group, String player, int amount, CommandSender sender) {
		if (amount < 1) {
			sender.sendMessage("Amount of XP must be a positive number!");
			return false;
		}
		
		if (_doesGroupRequireBanker(group) == true) {
			if (_isPlayerABanker(group, player) == true) {
				Player target = (Bukkit.getServer().getPlayer(player));
				if (target != null) {
					int balance = _getGroupXPBalance(group);
					if (balance > amount) {
						// Reduce the amount in the database
						if (_setGroupXPBalance(group, (amount * -1)) == true) {
							// Give the player the amount
							Player targetPlayer = Bukkit.getPlayer(player);
							float targetPlayerXP = targetPlayer.getExp();
							targetPlayer.setExp(targetPlayerXP + amount);
							_addTransaction(group, sender.getName(), "xp", "withdraw", amount);
							sender.sendMessage(String.format("Paid %s a total of %s XP", player, amount));
							eb.getLogger().info(String.format("%s paid %s %s XP", sender.getName(), player, amount));
							return true;
						} else {
							sender.sendMessage("Unable to deduct XP from database - halting transaction!");
							return false;
						}
					} else {
						// Not enough XP to cover the request!
						sender.sendMessage(String.format("Unable to transfer - Group %s only has %s available.", group, balance));
						return false;
					}
				} else {
					// Player is not online!
					sender.sendMessage(player + " is not online to recieve the transfer.");
					sender.sendMessage("Please try again when the player is online.");
					return false;
				}
			} else {
				// You're not a banker! Go away!
				sender.sendMessage("Permission denied - you are not a banker for this group bank!");
				return false;
			}
		} else {
			if (_isPlayerAOwner(group, player) == true ) {
				Player target = (Bukkit.getServer().getPlayer(player));
				if (target != null) {
					int balance = _getGroupXPBalance(group);
					if (balance > amount) {
						// Reduce the amount in the database
						if (_setGroupXPBalance(group, (amount * -1)) == true) {
							// Give the player the amount
							Player targetPlayer = Bukkit.getPlayer(player);
							float targetPlayerXP = targetPlayer.getExp();
							targetPlayer.setExp(targetPlayerXP + amount);
							_addTransaction(group, sender.getName(), "xp", "withdraw", amount);
							return true;
						} else {
							sender.sendMessage("Unable to deduct XP from database - halting transaction!");
							return false;
						}
					} else {
						// Not enough XP to cover the request!
						sender.sendMessage(String.format("Unable to transfer - Group %s only has %s XP available.", group, balance));
						return false;
					}
				} else {
					// Player is not online!
					sender.sendMessage(player + " is not online to recieve the transfer.");
					sender.sendMessage("Please try again when the player is online.");
					return false;
				}
			} else {
				// You're not an owner! Go away!
				sender.sendMessage("Permission denied - you are not the owner for this group bank!");
				return false;
			}
		}
	}

	// !!! Stubbed public method - need to complete!
	public static void PayMoneyToMember(String group, String player, Double amount, CommandSender sender) {
		// Use the same basic code from PayXPToMember (but, obviously, modify it to deal with money instead of XP)

		
		
	}
	
	// !!! Stubbed public method - need to complete!
	public static void DeleteGroup(String group, String player, CommandSender sender) {
		// Make sure group exists
		// Make sure the sender is the owner
		// Pay out all XP to sender via PayXPToMember()
		// Pay out all money to sender via PayMoneyToMember()
		// Delete all users from group_users where group_id = group
		// Update group to set is_active = false
		// ***** Waiting on PayXPToMember and PayMoneyToMember in order to build out this method
	}
	
	// !!! Stubbed public method - need to complete!
	public static void SetGroupFlags(String group, CommandSender sender, String flag, String value) {
		// Set the flag of a group to the given value
	}
	
	// !!! Stubbed public method - need to complete!
	public static void GetGroupFlags(String group, CommandSender sender, String flag) {
		// Check if sender is part of the group and display requested flag
		// (need to create an enumerated list of flags that can be worked with...)
	}
	
	// !!! Stubbed public method - need to complete!
	public static void GrantBanker(String group, String player, CommandSender sender) {
		// Check if sender is owner of the group
		// Change the 'is_banker' flag for the user and group to 1

	}
	
	// !!! Stubbed public method - need to complete!
	public static void RevokeBanker(String group, String player, CommandSender sender) {
		// Check if the sender is owner of the group
		// Change the 'is_banker' flag for the user and group to 0
	}
	
	// !!! Stubbed public method - need to complete!
	public static void ShowMembers(String group, CommandSender sender) {
		// If sender is part of group, show them the group members
	}
	
	// !!! Stubbed public method - need to complete!
	public static void AddMoney(String group, Double amount, CommandSender sender) {
		// If sender is part of group, then remove money from them and add money to group
	}
	
	// !!! Stubbed public method - need to complete!
	public static void AddXP(String group, Integer amount, CommandSender sender) {
		// If sender is part of group, then remove XP from them and add XP to group
	}
	
	// !!! Stubbed public method - need to complete!
	public static void ShowBalance(String group, CommandSender sender) {
		// If sender is part of group, get the XP and Money balance via existing private methods
	}

	// !!! Stubbed public method - need to complete!
	public static void ShowGroups(CommandSender sender) {
		// Show what groups the sender is a part of
	}

	// !!! Stubbed public method - need to complete!
	public static void ShowTransactions(String group, String[] parameters) {
		// This is going to be a big one...
		// Parse the parameters and check for player, days, action, and type and query the
		// database and send back the transactions matching those
		// Also need to make sure that the caller is an owner or banker and are part of the group being queried
	}

	
	
	
	// =============================================================================================================================================
	// Private methods in support of this data access class:
	//
	// Connection	_getConnection()							Returns a JDBC connection object to make queries against the DB with
	// void			_addHistory(user, group, action, info)		Adds an entry to the 'history' table for something having happened
	// void			_addTransaction(user, group, action, amt)	Adds an entry into the transactions table for financial history keeping
	// Boolean		_isPlayerAMember(group, player)				Returns boolean if a given player belongs to the given group
	// Boolean		_isPlayerAOwner(group, player)				Returns boolean if a given player is the owner of the given group
	// Boolean		_isPlayerABanker(group, player)				Returns boolean if a given player is a banker for the given group
	// Boolean		_isPlayerAnOwnerOrBanker(group, player)		Returns boolean if a given player is either an owner or banker for a group
	// Boolean		_isGroup(group)								Returns boolean if the given group is actually a group
	// Boolean		_doesGroupRequireBanker(group)				Returns boolean if the given group requires a banker to disburse funds
	// Integer		_getGroupXPBalance(group)					Returns the amount of XP in a group bank
	// Double		_getGroupMoneyBalance(group)				Returns the amount of money in a group bank
	// Boolean		_setGroupXPBalance(group)					Sets the XP balance in the group bank and returns boolean for status
	// Boolean		_setGroupMoneyBalance(group)				Sets the Money balance in the group bank and returns boolean for status
	// =============================================================================================================================================
	
	// Connection	_getConnection()							Returns a JDBC connection object to make queries against the DB with
	private static Connection _getConnection() {
		try {
			Class.forName(dbDriver);
			Connection conn = DriverManager.getConnection(String.format("jdbc:mysql://%s/%s", dbHostname, dbDatabase), dbUsername, dbPassword);
			return conn;
		}
		catch (Exception err) {
			eb.getLogger().warning(err.toString());
			return null;
		}
	}
	
	// void			_addHistory									Adds an entry to the 'history' table for something having happened
	private static void _addHistory(String group, String player, String action, String details) {

	}
	
	// void			_addTransaction(user, group, action, amt)	Adds an entry into the transactions table for financial history keeping
	private static void _addTransaction(String group, String player, String type, String action, double amount) {
		
		
	}
	
	// Boolean		_isPlayerAMember(group, player)				Returns boolean if a given player belongs to the given group
	private static boolean _isPlayerAMember(String group, String player) {
		String check_membership = "SELECT id FROM group_users WHERE name = ? AND group_id = (SELECT id FROM groups WHERE name = ?)";
		Connection conn = _getConnection();
		try {
			final PreparedStatement ps = conn.prepareStatement(check_membership);
			ps.setString(1, player);
			ps.setString(2, group);
			ResultSet rs = ps.executeQuery();
			int row_count = 0;
			while(rs.next()) {
				row_count++;
			}
			rs.close();
			conn.close();
			if (row_count == 1) {
				return true;
			} else {
				return false;
			}
		} catch (Exception err) {
			eb.getLogger().warning("Error while checking if player is a member:");
			eb.getLogger().warning(err.getMessage());
			return false;
		}
	}

	// Boolean		_isPlayerAOwner(group, player)				Returns boolean if a given player is the owner of the given group
	private static boolean _isPlayerAOwner(String group, String player) {
		String check_ownership = "SELECT id FROM group_users WHERE name = ? AND is_owner = true AND group_id = (SELECT id FROM groups WHERE name = ?)";
		Connection conn = _getConnection();
		try {
			final PreparedStatement ps = conn.prepareStatement(check_ownership);
			ps.setString(1, player);
			ps.setString(2, group);
			ResultSet rs = ps.executeQuery();
			int row_count = 0;
			while(rs.next()) {
				row_count++;
			}
			rs.close();
			conn.close();
			if (row_count == 1) {
				return true;
			} else {
				return false;
			}
		} catch (Exception err) {
			eb.getLogger().warning("Error while checking if player is a member:");
			eb.getLogger().warning(err.getMessage());
			return false;
		}
	}
	
	// Boolean		_isPlayerABanker(group, player)				Returns boolean if a given player is a banker for the given group
	private static boolean _isPlayerABanker(String group, String player) {
		String check_bankership = "SELECT id FROM group_users WHERE name = ? AND is_banker = true AND group_id = (SELECT id FROM groups WHERE name = ?)";
		Connection conn = _getConnection();
		try {
			final PreparedStatement ps = conn.prepareStatement(check_bankership);
			ps.setString(1, player);
			ps.setString(2, group);
			ResultSet rs = ps.executeQuery();
			int row_count = 0;
			while(rs.next()) {
				row_count++;
			}
			rs.close();
			conn.close();
			if (row_count == 1) {
				return true;
			} else {
				return false;
			}
		} catch (Exception err) {
			eb.getLogger().warning("Error while checking if player is a banker:");
			eb.getLogger().warning(err.getMessage());
			return false;
		}
	}

	// Boolean		_isPlayerAnOwnerOrBanker(group, player)		Returns boolean if a given player is either an owner or banker for a group
	private static boolean _isPlayerAnOwnerOrBanker(String group, String player) {
		String check_owner_or_banker = "SELECT id FROM group_users WHERE name = ? AND (is_banker = true OR is_owner = true) AND group_id = (SELECT id FROM groups WHERE name = ?)";
		Connection conn = _getConnection();
		try {
			final PreparedStatement ps = conn.prepareStatement(check_owner_or_banker);
			ps.setString(1, player);
			ps.setString(2, group);
			ResultSet rs = ps.executeQuery();
			int row_count = 0;
			while(rs.next()) {
				row_count++;
			}
			rs.close();
			conn.close();
			if (row_count > 0) {
				return true;
			} else {
				return false;
			}
		} catch (Exception err) {
			eb.getLogger().warning("Error while checking if player is an owner or banker:");
			eb.getLogger().warning(err.getMessage());
			return false;
		}
	}
	
	// Boolean		_isGroup(group)								Returns boolean if the given group is actually a group
	private static boolean _isGroup(String group) {
		String check_group = "SELECT id FROM groups WHERE name = ?";
		Connection conn = _getConnection();
		try {
			final PreparedStatement ps = conn.prepareStatement(check_group);
			ps.setString(1, group);
			ResultSet rs = ps.executeQuery();
			int row_count = 0;
			while(rs.next()) {
				row_count++;
			}
			rs.close();
			conn.close();
			if (row_count == 1){
				return true;
			} else {
				return false;
			}
		}
		catch (Exception err) {
			eb.getLogger().warning("Error while checking if group exists:");
			eb.getLogger().warning(err.getMessage());
			return false;
		}
	}
	
    // !!! Stubbed private method - need to complete!
	// Boolean		_doesGroupRequireBanker(group)				Returns boolean if the given group requires a banker to disburse funds
	private static boolean _doesGroupRequireBanker(String group) {
		
		
		
		return false;
	}
	
	// Integer		_checkGroupXPBalance					Returns the amount of XP in a group bank
	private static int _getGroupXPBalance(String group) {
		String check_xp_balance = String.format("SELECT xp FROM groups WHERE id = (SELECT id FROM groups WHERE name = %s) LIMIT 1", group);
		Connection conn = _getConnection();
		try {
			int myXP = 0;
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(check_xp_balance);
			rs.first();
			myXP = rs.getInt("xp");
			rs.close();
			conn.close();
			return myXP;
		}
		catch (Exception err) {
			eb.getLogger().warning("Error while checking XP balance of group " + group);
			eb.getLogger().warning(err.getMessage());
			return 0;
		}
	}
	
	// Double		_checkGroupMoneyBalance					Returns the amount of money in a group bank
	private static Double _getGroupMoneyBalance(String group) {
		String check_money_balance = String.format("SELECT money FROM groups WHERE id = (SELECT id FROM groups WHERE name = %s) LIMIT 1", group);
		Connection conn = _getConnection();
		try {
			double myMoney = 0;
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(check_money_balance);
			rs.first();
			myMoney = rs.getDouble("money");
			rs.close();
			conn.close();
			return myMoney;
		}
		catch (Exception err) {
			eb.getLogger().warning("Error while checking XP balance of group " + group);
			eb.getLogger().warning(err.getMessage());
			return 0.00;
		}

	}
	
	// Boolean		_setGroupXPBalance						Sets the XP balance in the group bank and returns boolean for status
	// The 'amount' parameter can also be a negative number (to decrement the amount) for withdrawls
	private static boolean _setGroupXPBalance(String group, int amount) {
		int balance = _getGroupXPBalance(group);
		balance = balance + amount;
		String update_balance = "UPDATE groups SET xp = ? WHERE id = (SELECT id FROM groups WHERE name = ?)";
		Connection conn = _getConnection();
		try {
			final PreparedStatement ps = conn.prepareStatement(update_balance);
			ps.setInt(1, balance);
			ps.setString(2, group);
			int rowsUpdated = ps.executeUpdate();
			if (rowsUpdated == 1) {
				return true;
			} else {
				return false;
			}
		}
		catch (Exception err) {
			eb.getLogger().warning("Error while trying to set the group XP balance for " + group);
			eb.getLogger().warning(err.getMessage());
			return false;
		}
	}
	
	// Boolean		_setGroupMoneyBalance					Sets the Money balance in the group bank and returns boolean for status
	// The 'amount parameter can also be a negative number (to decrement the amount) for withdrawls
	private static boolean _setGroupMoneyBalance(String group, int amount) {
		double balance = _getGroupMoneyBalance(group);
		balance = balance + amount;
		String update_balance = "UPDATE groups SET money = ? WHERE id = (SELECT id FROM groups WHERE name = ?)";
		Connection conn = _getConnection();
		try {
			final PreparedStatement ps = conn.prepareStatement(update_balance);
			ps.setDouble(1, balance);
			ps.setString(2, group);
			int rowsUpdated = ps.executeUpdate();
			if (rowsUpdated == 1) {
				return true;
			} else {
				return false;
			}
		}
		catch (Exception err) {
			eb.getLogger().warning("Error while trying to set the group money balance for " + group);
			eb.getLogger().warning(err.getMessage());
			return false;
		}
	}

	
}
